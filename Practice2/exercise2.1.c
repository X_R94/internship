#include <stdio.h>
#include <time.h>
#define G 9.81F

int main()
{
    float H, L, h;
    int resCode;
    int T = 0;
    clock_t time;
	
    puts("Type H:");
    resCode = scanf("%f", &H);
    if (resCode != 1) {
        puts("Incorrect format");
        return -1;
    }
    if (H < 0) {
        puts("Must be positive");
        return -1;
    }

    while (1)
    {
        time = clock();
        L = G * T * T / 2;
        h = H - L;
        if (h <= 0) {
            puts("Done!!!!!");
            break;
        }
        printf("%02ds %.1fm\n", T, h);
        while (clock() < time + 1000);
        T++;
    }
    return 0;
}