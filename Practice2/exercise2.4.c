#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main()
{
    char str[] = { "423Test19am204aaf2aa2" };
    int n = strlen(str) - 1;
    int i, changed;
    char tmp;

    printf("Before: %s\n", str);
    do {
        changed = 0;
        for (i = 0; i < n; i++) 
        {
            if (isdigit(str[i]))
            {
                if (isalpha(str[i + 1]))
                {
                    tmp = str[i];
                    str[i] = str[i + 1];
                    str[i + 1] = tmp;
                    changed = 1;
                }
            }
        }
    } while (changed);
    printf("After:  %s\n", str);

    return 0;
}