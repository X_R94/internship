#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int lineCount, starCount, offset;
    int i, j;
    int resCode;

    srand(time(0));
    puts("Type the number of lines:");
    resCode = scanf("%d", &lineCount);
    if (resCode != 1) {
        puts("Incorrect format");
        return -1;
    }

    for (i = 0; i < lineCount; i++)
    {
        starCount = 2 * i + 1;
        offset = lineCount - i - 1;
        for (j = 0; j < offset; j++) {
            putchar(' ');
        }
        for (j = 0; j < starCount; j++) {
            putchar(rand() % 2 + 1);
        }
        putchar('\n');
    }
    return 0;
}