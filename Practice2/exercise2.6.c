#include <stdio.h>
#include <string.h>
#define STRMAX 256

int main()
{
    char str[STRMAX];
    int n;
    int i, j, start = 0;

    fgets(str, STRMAX, stdin);
    n = strlen(str);
    str[n - 1] = '\0';
    printf("Before: '%s'\n", str);
	
    for (i = 0; i < n; i++) 
    {
        if (str[i] != ' ') 
            start = 1;
        else 
        {
            if ((start && str[i + 1] == ' ') || !start) 
            {
                for (j = i; j < n; j++) {
                    str[j] = str[j + 1];
                }
                i--;
            }
        }
    }

    n = strlen(str) - 1;
    if (str[n] == ' ')
        str[n] ='\0';

    printf("After:  '%s'\n", str);
    return 0;
}