#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define STRMAX 256

int main()
{
    int i, n;
    int charCode;
    char letter;
    char str[STRMAX];
    int table[256] = { 0 }; // using ASCII table

    puts("Type some text:");
    fgets(str, STRMAX, stdin);
    n = strlen(str);

    for (i = 0; i < n; i++)
    {
        charCode = str[i];
        table[charCode]++;
    }
    
    for (i = 0; i < 256; i++)
    {
        if (table[i] != 0 && isprint(i)) {
            letter = i;
            printf("%c: %d\n", i, table[i]);
        }
    }
    return 0;
}