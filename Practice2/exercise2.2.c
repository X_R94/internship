#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int number, input, tries = 0;
    int resCode;

    srand(time(0));
    number = rand() % 100;
    puts("Try to get the number (0..100):");
    while (1)
    {
        fflush(stdin);
        resCode = scanf("%d", &input);
        if (resCode != 1) {
            puts("Incorrect format");
            break;
        }
        if (input < 0 || input > 100) {
            puts("Must be between 0 and 100. Try again:");
            continue;
        }
        tries++;
        if (input == number) {
            puts("That's right!");
            printf("Tries: %d\n", tries);
            break;
        }
        else if (input < number)
            puts("More");
        else
            puts("Less");
    }
    return 0;
}