#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

int main()
{
    int charCode, isDigit, isLower;
    int i, j;

    srand(time(0));
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 8; j++)
        {
            isDigit = rand() % 2;
            if (isDigit) 
                putchar(rand() % 10 + 48); // '0' is on the 48th position (ASCII)
            else 
            {
                charCode = rand() % 26 + 65; // 'A' is on the 65th position (ASCII)
                isLower = rand() % 2;
                if (isLower)
                    charCode = tolower(charCode);
                putchar(charCode);
            }
        }
        putchar('\n');
    }
    return 0;
}