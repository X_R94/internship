#include <stdio.h>
#include <ctype.h>
#define STRMAX 256

int main()
{
    char str[STRMAX];
    int i = 0;
    int inWord = 0;
    int count = 0;

    puts("Type some text:");
    fgets(str, STRMAX, stdin);

    while (str[i])
    {
        if (isalnum(str[i]) && inWord == 0)
        {
            count++;
            inWord = 1;
        }
        else if (!isalnum(str[i]) && inWord == 1) 
            inWord = 0;
        i++;
    }
    printf("Word count: %d\n", count);

    return 0;
}