#include <stdio.h>
#define STRMAX 256

int main()
{
    char str[STRMAX];
    int length = 1, start = 0,  maxLength = 1, maxStart = 0;
    char last;
    int i = 1;

    puts("Type some text:");
    fgets(str, STRMAX, stdin);

    last = str[0];
    while (str[i])
    {
        if (str[i] == last)
            length++;
        else 
        {
            if (length > maxLength) {
                maxLength = length;
                maxStart = start;
            }
            start = i;
            length = 1;
        }
        last = str[i];
        i++;
    }

    printf("%d ", maxLength);
    for (i = maxStart; i < maxStart + maxLength; i++)
        putchar(str[i]);
    putchar('\n');

    return 0;
}