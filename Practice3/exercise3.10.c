#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define STRMAX 256

int main()
{
    char str[STRMAX];
    int inWord = 0;
    int n, count = 0, start, wordLength, strLength;
    int i = 0, j, resCode;

    puts("Type some text:");
    fgets(str, STRMAX, stdin);

    fflush(stdin);
    puts("Number of word:");
    resCode = scanf("%d", &n); 
    if (resCode != 1) {
        puts("Incorrect input");
        return -1;
    }
    if (n <= 0) {
        puts("Must be positive");
        return -1;
    }

    strLength = strlen(str) - 1;
    while (str[i])
    {
        if (isalnum(str[i]) && inWord == 0)
        {
            count++;
            if (n == count)
                start = i;
            inWord = 1;
        }
        else if (!isalnum(str[i]) && inWord == 1)
        {
            inWord = 0;
            if (n == count)
            {
                wordLength = i - start;
                for (j = i; j <= strLength; j++)
                    str[j - wordLength] = str[j];
                for (j = strLength; j >= strLength - wordLength; j--)
                    str[j] = '\0';
                puts("Done:");
                puts(str);
                return 0;
            }
        }
        i++;
    }

    puts("The number is too big");
    return 0;
}