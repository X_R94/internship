#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int n, sum = 0;
    int max = 0, min = 0, start = 0, end = 0;
    int i, resCode;
    int *a;

    srand(time(0));
    puts("Type N:");
    resCode = scanf("%d", &n);
    if (resCode != 1) {
        puts("Incorrect input");
	    return -1;
    }
    if (n <= 0) {
        puts("Must be positive");
        return -1;
    }
	
    a = (int *)malloc(n * sizeof(int));
    puts("Array:");
    for (i = 0; i < n; i++)
    {
        a[i] = rand() % 9;
        if (rand() % 2)
            a[i] = -(a[i]);

        printf("%d ", a[i]);
    }

    for (i = 0; i < n; i++)
    {
        if (a[i] > a[max])
            max = i;
        if (a[i] < a[min])
            min = i;
    }

    if (max - min > 1) {
        start = min + 1;
        end = max;
    }
    if (min - max > 1) {
        start = max + 1;
        end = min;
    }
    for (i = start; i < end; i++)
        sum += a[i];

    printf("\nSum: %d\n", sum);
    free(a);
    return 0;
}