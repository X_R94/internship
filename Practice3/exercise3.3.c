#include <stdio.h>
#include <ctype.h>
#define STRMAX 256

int main()
{
    char str[STRMAX];
    int i = 0;
    int inWord = 0;
    int start, length, maxStart, maxLength = 0;

    puts("Type some text:");
    fgets(str, STRMAX, stdin);

    while (str[i])
    {
        if (isalnum(str[i]))
        {
            if (!inWord) {
                inWord = 1;
                start = i;
                length = 1;
            }
            else
                length++;
        }
        else if (!isalnum(str[i]) && inWord == 1) 
        {
            inWord = 0;
            if (length > maxLength) {
                maxStart = start;
                maxLength = length;
            }
        }
        i++;
    }

    for (i = maxStart; i < maxStart + maxLength; i++) {
        putchar(str[i]);
    }

    if (maxLength)
        printf(" %d\n", maxLength);
    else
        puts("No words were found");

    return 0;
}