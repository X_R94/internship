#include <stdio.h>
#include <ctype.h>
#define STRMAX 256

int main()
{
    int i = 0, j;
    int charCode, length = 0, isFound = 0;
    char str[STRMAX];
    int table[256][2];
    int count;

    puts("Type some text:");
    fgets(str, STRMAX, stdin);

    while (str[i])
    {
        isFound = 0;
        charCode = str[i];
        for (j = 0; j < length; j++)
            if (charCode == table[j][0]) {
                isFound = 1;
                table[j][1]++;
                break;
            }

        if (!isFound)
        {
            table[length][0] = charCode;
            table[length][1] = 1;
            length++;
        }

        i++;
    }
	
    //bubble sort
    for (i = 1; i < length; i++)
        for (j = length - 1; j >= i; j--)
            if (table[j - 1][1] < table[j][1]) 
            {
                charCode = table[j][0];
                count = table[j][1];
                table[j][0] = table[j - 1][0];
                table[j][1] = table[j - 1][1];
                table[j - 1][0] = charCode;
                table[j - 1][1] = count;
            }

    for (j = 0; j < length; j++) {
        if (isprint(table[j][0]))
            printf("%c: %d\n", table[j][0], table[j][1]);
    }
    return 0;
}
