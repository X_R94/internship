#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#define STRMAX 256
#define NMAX 7

char str[STRMAX], number[NMAX];
int inNumber = 0;
int start, length;
long sum = 0;

void add_number()
{
    int j, k;
    inNumber = 0;
    for (k = 0; k < NMAX; k++)
        number[k] = '0';
    k = NMAX - 1;
    for (j = start + length - 1; j >= start; j--) {
        number[k] = str[j];
        k--;
    }
    sum += atol(number);
}

int main()
{
    int i = 0;

    puts("Type some text:");
    fgets(str, STRMAX, stdin);

    while (str[i])
    {
        if (isdigit(str[i]))
        {
            if (!inNumber) 
            {
                inNumber = 1;
                start = i;
                length = 1;
            }
            else
            {
                length++;
                if (length == NMAX)
                    add_number();
            }
        }
        else if (!isdigit(str[i]) && inNumber == 1) 
            add_number();
        i++;
    }
    printf("Sum: %ld\n", sum);
	
    return 0;
}