#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int n, sum = 0;
    int firstNegative = -1, lastPositive = -1;
    int i, resCode;
    int *a;

    srand(time(0));
    puts("Type N:");
    resCode = scanf("%d", &n);
    if (resCode != 1) {
        puts("Incorrect input");
        return -1;
    }
    if (n <= 0) {
        puts("Must be positive");
        return -1;
    }
	
    a = (int *)malloc(n * sizeof(int));
    puts("Array:");
    for (i = 0; i < n; i++)
    {
        a[i] = rand() % 9;
        if (rand() % 2)
            a[i] = -(a[i]);

        printf("%d ", a[i]);
    }

    for (i = 0; i < n; i++)
    {
        if (a[i] < 0 && firstNegative == -1)
            firstNegative = i;
        else if (a[i] > 0)
            lastPositive = i;
    }

    if (firstNegative != -1 && lastPositive != -1)
        for (i = firstNegative + 1; i < lastPositive; i++)
            sum += a[i];

    printf("\nSum: %d\n", sum);
    free(a);
    return 0;
}