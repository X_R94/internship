#include <stdio.h>
#include <ctype.h>
#define STRMAX 256

int main()
{
    char str[STRMAX];
    int inWord = 0;
    int n, count = 0, start;
    int i = 0, j, resCode;

    puts("Type some text:");
    fgets(str, STRMAX, stdin);

    fflush(stdin);
    puts("Number of word:");
    resCode = scanf("%d", &n); 
    if (resCode != 1) {
        puts("Incorrect input");
        return -1;
    }
    if (n <= 0) {
        puts("Must be positive");
        return -1;
    }

    while (str[i])
    {
        if (isalnum(str[i]) && inWord == 0)
        {
            count++;
            if (n == count)
                start = i;
            inWord = 1;
        }
        else if (!isalnum(str[i]) && inWord == 1)
        {
            inWord = 0;
            if (n == count)
            {
                puts("Word:");
                for (j = start; j < i; j++)
                    putchar(str[j]);
                putchar('\n');
                return 0;
            }
        }
        i++;
    }
    puts("The number is too big");
	
    return 0;
}