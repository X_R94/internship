#include <stdio.h>
#include <ctype.h>
#define PI 3.14F

int main()
{
    float angle;
    float degrees, radians;
    char format;
    int resCode;
    
    puts("Value:");
    resCode = scanf("%f%c", &angle, &format);
    if (resCode != 2) {
        puts("Input error");
        return -1;
    }
	
    format = toupper(format);
    switch (format)
    {
        case 'D':
            radians = angle * PI / 180;
            printf("Radians: %.2f\n", radians);
            break;
        case 'R':
            degrees = angle * 180 / PI;
            printf("Degrees: %.2f\n", degrees);
            break;
        default:
            puts("Incorrect format");
    }
    return 0;
}