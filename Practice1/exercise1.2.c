#include <stdio.h>

int main()
{
    int resCode;
    int h, m, s;
    char time[8];

    puts("Type current time (HH:MM:SS):");
    resCode = scanf("%d:%d:%d", &h, &m, &s);
    if (resCode != 3) {
        puts("Incorrect format.");
        return -1;
    }
    if (h > 23 || h < 0) {
        puts("HH incorrect format: must be between 0 and 23.");
        return -1;
    }
    if (m > 59 || m < 0) {
        puts("MM incorrect format: must be between 0 and 59.");
        return -1;
    }
    if (s > 59 || s < 0) {
        puts("SS incorrect format: must be between 0 and 59.");
        return -1;
    }

    if (h < 6 || h == 23)
        puts("Good night");
    else if (h < 12)
        puts("Good morning");
    else if (h < 18)
        puts("Good afternoon");
    else if (h < 23)
        puts("Good evening");

    return 0;
}
