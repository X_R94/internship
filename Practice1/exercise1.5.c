#include <stdio.h>
#include <string.h>
#define LINE 81
#define STRMAX 256

int main()
{
    char str[STRMAX];
    int n, offset;
    int i;

    puts("Type some text:");
    fgets(str, STRMAX, stdin);
    n = strlen(str);
    offset = (LINE - n) / 2;
    for (i = 0; i < offset; i++) {
        putchar(' ');
    }
    puts(str);
    
    return 0;
}