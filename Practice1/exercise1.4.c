#include <stdio.h>
#include <ctype.h>
#define PI 3.14F

int main()
{
    float sm;
    int feet, inches;
    int resCode;
    
    puts("Feet:");
    resCode = scanf("%d", &feet);
    if (resCode != 1) {
        puts("Input error");
        return -1;
    }

    fflush(stdin);
    puts("Inches:");
    resCode = scanf("%d", &inches);
    if (resCode != 1) {
        puts("Input error");
        return -1;
    }
	
    if (feet < 0 || inches < 0) {
        puts("Must be positive");
        return -1;
    }

    sm = (feet * 12 + inches) * 2.54F;
    printf("Santimeters: %.1f\n", sm);

    return 0;
}
