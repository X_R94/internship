#include <stdio.h>
#include <math.h>
#include <ctype.h>

int main()
{
    char gender;
    int height, weight;
    int ideal, difference;
    int resCode;

    puts("Type your height:");
    resCode = scanf("%d", &height);
    if (height <= 0 || resCode != 1) {
        puts("Incorrect height");
        return -1;
    }
    
    fflush(stdin);
    puts("Type your weight:");
    resCode = scanf("%d", &weight);
    if (weight <= 0 || resCode != 1) {
        puts("Incorrect weight");
        return -1;
    }
    
    fflush(stdin);
    puts("Choose your gender (m/f):");
    scanf("%c", &gender);
    gender = toupper(gender);
    
    switch (gender)
    {
        case 'M':
            ideal = height - 100;
            break;
        case 'F':
            ideal = height - 110;
            break;
        default:
            puts("Incorrect gender");
            return -1;
    }

    difference = ideal - weight;
    if (abs(difference) <= 5)
        puts("It's OK.");
    else if (difference < -5)
        puts("It's better to loose some weight.");
    else if (difference > 5)
        puts("It's better to gain some weight.");

    return 0;
}
